//
//  ViewController.m
//  vc
//
//  Created by Ansis on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "GPUImage.h"

@interface ViewController (){
    GPUImageVideoCamera *videoCamera;
    GPUImageView *filteredVideoView;
    GPUImageFilter  *customFilter;
    
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack];
    videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
    customFilter = [[GPUImageFilter alloc] initWithFragmentShaderFromFile:@"CustomFilter"];
///filter = [[GPUImageSepiaFilter alloc] init];
    filteredVideoView = [[GPUImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self.view addSubview: filteredVideoView];

    [self.view addSubview: filteredVideoView];
//    [videoCamera addTarget:filteredVideoView];
    
    [videoCamera addTarget:customFilter];
    [customFilter addTarget:filteredVideoView];
    
    [videoCamera startCameraCapture];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
